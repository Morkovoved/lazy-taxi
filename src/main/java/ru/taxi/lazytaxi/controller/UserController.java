package ru.taxi.lazytaxi.controller;

import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import ru.taxi.lazytaxi.model.User;
import ru.taxi.lazytaxi.service.UserService;

import org.springframework.security.crypto.password.PasswordEncoder;


@RestController
@RequestMapping(value = "/users", produces = MediaType.APPLICATION_JSON_VALUE)
@Validated
@Secured({"ROLE_USER", "ROLE_ADMIN"})
@Slf4j
//@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserController {
	private PasswordEncoder passwordEncoder;
	private UserService userService;

	public UserController(PasswordEncoder passwordEncoder, UserService userService) {
		this.passwordEncoder = passwordEncoder;
		this.userService = userService;
	}

	@PostMapping
	@ResponseStatus(HttpStatus.CREATED)
	public void save(@RequestBody User user) {
		user.setPassword(passwordEncoder.encode(user.getPassword()));
		userService.save(user);
	}

	@PutMapping("/{userId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void save(@PathVariable Integer userId, @RequestBody User user) {
		user.setId(userId);
		userService.save(user);
	}

	@DeleteMapping("/{userId}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void delete(@PathVariable Integer userId) {
		userService.delete(userId);
	}

	@GetMapping("/{userId}")
	@ResponseStatus(HttpStatus.OK)
	public User get(@PathVariable Integer userId) {
		return userService.find(userId);
	}

//    @GetMapping("/search")
//    public User searchByEmail(@RequestParam("email") String email) {
//        return userService.findByEmail(email);
//    }

	@GetMapping("/search")
	@ResponseStatus(HttpStatus.OK)
	public User searchByUsername(@RequestParam("username") String username) {
		return userService.findByUsername(username);
	}

	@GetMapping("/")
	@ResponseStatus(HttpStatus.OK)
	public String getById() {
		return "Hello !!! " + passwordEncoder.encode("KyleBroflovski");
	}

//    @GetMapping("/")
//    public List<User> getById() {
//        return userService.findAll();
//    }

//    @GetMapping("/search")
//    public List<User> searchByEnabled(@RequestParam Boolean enabled) {
//        return userService.findByEnabled(enabled);
//    }
//
//    @GetMapping("/search")
//    public List<User> searchByName(@RequestParam String name) {
//        return userService.findByName(name);
//    }
}
