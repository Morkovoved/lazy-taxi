package ru.taxi.lazytaxi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.PathSelectors;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.*;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spi.service.contexts.SecurityContext;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

@Configuration
@EnableSwagger2
public class SwaggerConfig {
	@Bean
	public Docket docket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.select()
				.apis(RequestHandlerSelectors.basePackage("ru.taxi.lazytaxi"))
				.paths(PathSelectors.any())
				.build()
				.securityContexts(Collections.singletonList(securityContext()))
				.securitySchemes(Collections.singletonList(oauthSecuritySchema()));
	}

	private OAuth oauthSecuritySchema() {
		List<AuthorizationScope> authorizationScopeList = Arrays.asList(
				new AuthorizationScope("read", "read"),
				new AuthorizationScope("write", "write")
		);
		List<GrantType> grantTypes = Collections.singletonList(new ResourceOwnerPasswordCredentialsGrant("/oauth/token"));
		return new OAuth("oauth2", authorizationScopeList, grantTypes);
	}

	private SecurityContext securityContext() {
		return SecurityContext.builder().securityReferences(Collections.singletonList(securityReference())).build();
	}


	private SecurityReference securityReference() {
		return new SecurityReference("oauth2", new AuthorizationScope[]{});
	}
}


