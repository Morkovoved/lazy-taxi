package ru.taxi.lazytaxi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LazyTaxiApplication {

    public static void main(String[] args) {
        SpringApplication.run(LazyTaxiApplication.class, args);
    }
}
