package ru.taxi.lazytaxi.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "\"role\"")
public class Role {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column(name = "role_id")
	private Integer roleId;
	private String name;

	@Override
	public boolean equals(Object o) {
		if (this == o) return true;
		if (!(o instanceof Role)) return false;

		Role role = (Role) o;

		if (getRoleId() != null ? !getRoleId().equals(role.getRoleId()) : role.getRoleId() != null) return false;
		return getName() != null ? getName().equals(role.getName()) : role.getName() == null;
	}

	@Override
	public int hashCode() {
		int result = getRoleId() != null ? getRoleId().hashCode() : 0;
		result = 31 * result + (getName() != null ? getName().hashCode() : 0);
		return result;
	}

	@Override
	public String toString() {
		return "Role{" +
				"roleId=" + roleId +
				", name='" + name + '\'' +
				'}';
	}
}

