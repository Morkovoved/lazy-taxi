package ru.taxi.lazytaxi.model;

import javax.persistence.*;
import java.util.Date;

import lombok.Data;

@Data
@Entity
@Table(name = "\"order\"")
public class Order {
    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    @Column(name = "order_id")
    private Long orderId;
    private String clientPhone;
    @ManyToOne
    @JoinColumn(name = "client_id")
    private User client;
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "driver_id")
    private Driver driver;
    @ManyToOne
    @JoinColumn(name = "order_state_id")
    private OrderState orderState;
    private Date createdDate;
    private Date startedDate;
    private Integer priority;
    private Integer amount;
    private String comment;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Order)) return false;

        Order order = (Order) o;

        if (orderId != null ? !orderId.equals(order.orderId) : order.orderId != null) return false;
        if (clientPhone != null ? !clientPhone.equals(order.clientPhone) : order.clientPhone != null) return false;
        if (client != null ? !client.equals(order.client) : order.client != null) return false;
        if (driver != null ? !driver.equals(order.driver) : order.driver != null) return false;
        if (orderState != null ? !orderState.equals(order.orderState) : order.orderState != null) return false;
        if (createdDate != null ? !createdDate.equals(order.createdDate) : order.createdDate != null) return false;
        if (startedDate != null ? !startedDate.equals(order.startedDate) : order.startedDate != null) return false;
        if (priority != null ? !priority.equals(order.priority) : order.priority != null) return false;
        if (amount != null ? !amount.equals(order.amount) : order.amount != null) return false;
        return comment != null ? comment.equals(order.comment) : order.comment == null;
    }

    @Override
    public int hashCode() {
        int result = orderId != null ? orderId.hashCode() : 0;
        result = 31 * result + (clientPhone != null ? clientPhone.hashCode() : 0);
        result = 31 * result + (client != null ? client.hashCode() : 0);
        result = 31 * result + (driver != null ? driver.hashCode() : 0);
        result = 31 * result + (orderState != null ? orderState.hashCode() : 0);
        result = 31 * result + (createdDate != null ? createdDate.hashCode() : 0);
        result = 31 * result + (startedDate != null ? startedDate.hashCode() : 0);
        result = 31 * result + (priority != null ? priority.hashCode() : 0);
        result = 31 * result + (amount != null ? amount.hashCode() : 0);
        result = 31 * result + (comment != null ? comment.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Order{" +
                "orderId=" + orderId +
                ", clientPhone='" + clientPhone + '\'' +
                ", client=" + client +
                ", driver=" + driver +
                ", orderState=" + orderState +
                ", createdDate=" + createdDate +
                ", startedDate=" + startedDate +
                ", priority=" + priority +
                ", amount=" + amount +
                ", comment='" + comment + '\'' +
                '}';
    }
}
