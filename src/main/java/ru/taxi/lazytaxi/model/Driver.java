package ru.taxi.lazytaxi.model;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

import lombok.Data;

@Data
@Entity
@Table(name = "driver")
public class Driver extends User {
    private String driverLicense;
    @ManyToOne
    @JoinColumn(name = "driver_state_id")
    private DriverState driverState;
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "car_id")
    private Car car;
    private Integer priority;
    private Integer radiusWork;
    private Boolean enabled;
    @OneToMany(mappedBy = "driver", fetch = FetchType.LAZY)
    private Set<Order> orders = new HashSet<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Driver)) return false;
        if (!super.equals(o)) return false;

        Driver driver = (Driver) o;

        if (!getDriverLicense().equals(driver.getDriverLicense())) return false;
        if (getDriverState() != null ? !getDriverState().equals(driver.getDriverState()) : driver.getDriverState() != null)
            return false;
        if (getCar() != null ? !getCar().equals(driver.getCar()) : driver.getCar() != null) return false;
        if (getPriority() != null ? !getPriority().equals(driver.getPriority()) : driver.getPriority() != null)
            return false;
        if (getRadiusWork() != null ? !getRadiusWork().equals(driver.getRadiusWork()) : driver.getRadiusWork() != null)
            return false;
        return getEnabled().equals(driver.getEnabled());
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        result = 31 * result + getDriverLicense().hashCode();
        result = 31 * result + (getDriverState() != null ? getDriverState().hashCode() : 0);
        result = 31 * result + (getCar() != null ? getCar().hashCode() : 0);
        result = 31 * result + (getPriority() != null ? getPriority().hashCode() : 0);
        result = 31 * result + (getRadiusWork() != null ? getRadiusWork().hashCode() : 0);
        result = 31 * result + getEnabled().hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Driver{" +
                "driverLicense='" + driverLicense + '\'' +
                ", driverState=" + driverState +
                ", car=" + car +
                ", priority=" + priority +
                ", radiusWork=" + radiusWork +
                ", enabled=" + enabled +
                "} " + super.toString();
    }
}
