package ru.taxi.lazytaxi.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "driver_state")
public class DriverState {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer driverStateId;
    private String name;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DriverState)) return false;
        if (o == null || getClass() != o.getClass()) return false;

        DriverState that = (DriverState) o;

        if (driverStateId != null ? !driverStateId.equals(that.driverStateId) : that.driverStateId != null)
            return false;
        return name != null ? name.equals(that.name) : that.name == null;
    }

    @Override
    public int hashCode() {
        int result = driverStateId != null ? driverStateId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "DriverState{" +
                "driverStateId=" + driverStateId +
                ", name='" + name + '\'' +
                '}';
    }
}
