package ru.taxi.lazytaxi.model;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "order_state")
public class OrderState {
    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private Integer orderStateId;
    private String name;
    private Boolean finalState;

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof OrderState)) return false;
        if (o == null || getClass() != o.getClass()) return false;

        OrderState that = (OrderState) o;

        if (orderStateId != null ? !orderStateId.equals(that.orderStateId) : that.orderStateId != null) return false;
        if (name != null ? !name.equals(that.name) : that.name != null) return false;
        return finalState != null ? finalState.equals(that.finalState) : that.finalState == null;
    }

    @Override
    public int hashCode() {
        int result = orderStateId != null ? orderStateId.hashCode() : 0;
        result = 31 * result + (name != null ? name.hashCode() : 0);
        result = 31 * result + (finalState != null ? finalState.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "OrderState{" +
                "orderStateId=" + orderStateId +
                ", name='" + name + '\'' +
                ", finalState=" + finalState +
                '}';
    }
}
