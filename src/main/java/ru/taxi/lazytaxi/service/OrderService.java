package ru.taxi.lazytaxi.service;

import ru.taxi.lazytaxi.model.Order;

public interface OrderService extends CrudService<Order, Long> {
}
