package ru.taxi.lazytaxi.service;

import ru.taxi.lazytaxi.model.Driver;

public interface DriverService extends CrudService<Driver, Integer> {
}
