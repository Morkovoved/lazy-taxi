package ru.taxi.lazytaxi.service;

import ru.taxi.lazytaxi.model.Car;

public interface CarService extends CrudService<Car, Integer> {
}
