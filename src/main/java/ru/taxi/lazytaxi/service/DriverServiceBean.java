package ru.taxi.lazytaxi.service;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.taxi.lazytaxi.model.Driver;
import ru.taxi.lazytaxi.exception.DriverNotFoundException;
import ru.taxi.lazytaxi.repository.DriverRepository;

import javax.annotation.PostConstruct;


@Service
@Slf4j
//@AllArgsConstructor(onConstructor = @__(@Autowired))
public class DriverServiceBean implements DriverService {
    private DriverRepository driverRepository;

    public DriverServiceBean(DriverRepository driverRepository) {
        this.driverRepository = driverRepository;
    }

    @PostConstruct
    public void init() { }

    @Transactional
    @Override
    public Driver save(Driver driver) {
        if (driver.getId() != null && !driverRepository.existsById(driver.getId())) {
            throw new DriverNotFoundException();
        }
        return driverRepository.save(driver);
    }

    @Override
    public Driver find(Integer id) {
        return driverRepository.findById(id).orElseThrow(DriverNotFoundException::new);
    }

    @Override
    public List<Driver> findAll() {
        return driverRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        driverRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        driverRepository.deleteAll();
    }
}
