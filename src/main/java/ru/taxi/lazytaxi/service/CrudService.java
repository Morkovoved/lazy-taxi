package ru.taxi.lazytaxi.service;

import java.util.List;

public interface CrudService <T, ID> {
	T save(T object);
	T find(ID id);
	List<T> findAll();
	void delete(ID id);
	void deleteAll();
}
