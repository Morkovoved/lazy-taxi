package ru.taxi.lazytaxi.service;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.taxi.lazytaxi.model.Car;
import ru.taxi.lazytaxi.exception.CarNotFoundException;
import ru.taxi.lazytaxi.repository.CarRepository;

import org.springframework.stereotype.Service;

@Service
@Slf4j
//@AllArgsConstructor(onConstructor = @__(@Autowired))
public class CarServiceBean implements CarService {
    private CarRepository carRepository;

    public CarServiceBean(CarRepository carRepository) {
        this.carRepository = carRepository;
    }

    @Transactional
    @Override
    public Car save(Car car) {
        if (car.getCarId() != null && !carRepository.existsById(car.getCarId())) {
            throw new CarNotFoundException();
        }
        return carRepository.save(car);
    }

    @Override
    public Car find(Integer id) {
        return carRepository.findById(id).orElseThrow(CarNotFoundException::new);
    }

    @Override
    public List<Car> findAll() {
        return carRepository.findAll();
    }

    @Override
    public void delete(Integer id) {
        carRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        carRepository.deleteAll();
    }
}
