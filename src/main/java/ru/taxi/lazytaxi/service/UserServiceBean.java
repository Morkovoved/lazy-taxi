package ru.taxi.lazytaxi.service;

import java.util.Date;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.taxi.lazytaxi.model.User;
import ru.taxi.lazytaxi.exception.UserNotFoundException;
import ru.taxi.lazytaxi.repository.UserRepository;

import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
@Slf4j
//@AllArgsConstructor(onConstructor = @__(@Autowired))
public class UserServiceBean implements UserService {
    private UserRepository userRepository;

    public UserServiceBean(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Transactional
    @Override
    public User save(User user) {
        if (user.getId() != null && !userRepository.existsById(user.getId())) {
            throw new UserNotFoundException();
        }
        return userRepository.save(user);
    }

    @Override
    public User find(Integer id) {
        return userRepository.findById(id).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public void delete(Integer userId) {
        userRepository.deleteById(userId);
    }

    @Override
    public void deleteAll() {
        userRepository.deleteAll();
    }

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username).orElseThrow(() -> new UsernameNotFoundException(username));
    }

    @Override
    public User findByEmail(String email) {
        return userRepository.findByUsername(email).orElseThrow(UserNotFoundException::new);
    }

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public List<User> findByName(String name) {
        return userRepository.findByName(name);
    }

    @Override
    public List<User> findByNameLike(String name) {
        return userRepository.findByNameLike(name);
    }

    @Override
    public List<User> findBySurname(String surname) {
        return userRepository.findBySurname(surname);
    }

    @Override
    public List<User> findByNameAndSurname(String name, String surname) {
        return userRepository.findByNameAndSurname(name, surname);
    }

    @Override
    public List<User> findByEnabled(Boolean enabled) {
        return userRepository.findByEnabled(enabled);
    }

    @Override
    public List<User> findByCreatedDateAfter(Date startDate) {
        return userRepository.findByCreatedDateAfter(startDate);
    }
}
