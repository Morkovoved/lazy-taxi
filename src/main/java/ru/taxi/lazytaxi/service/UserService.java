package ru.taxi.lazytaxi.service;

import java.util.Date;
import java.util.List;

import ru.taxi.lazytaxi.model.User;

public interface UserService extends CrudService<User, Integer> {
	User findByUsername(String username);
	User findByEmail(String email);
	List<User> findAll();
	List<User> findByName(String name);
	List<User> findByNameLike(String name);
	List<User> findBySurname(String surname);
	List<User> findByNameAndSurname(String name, String surname);
	List<User> findByEnabled(Boolean enabled);
	List<User> findByCreatedDateAfter(Date startDate);
}
