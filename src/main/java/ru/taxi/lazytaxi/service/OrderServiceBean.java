package ru.taxi.lazytaxi.service;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import ru.taxi.lazytaxi.model.Order;
import ru.taxi.lazytaxi.exception.OrderNotFoundException;
import ru.taxi.lazytaxi.repository.OrderRepository;

import org.springframework.stereotype.Service;

@Service
@Slf4j
//@AllArgsConstructor(onConstructor = @__(@Autowired))
public class OrderServiceBean implements OrderService {
    private OrderRepository orderRepository;

    public OrderServiceBean(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }

    @Transactional
    @Override
    public Order save(Order order) {
        if (order.getOrderId() != null && !orderRepository.existsById(order.getOrderId())) {
            throw new OrderNotFoundException();
        }
        return orderRepository.save(order);
    }

    @Override
    public Order find(Long id) {
        return orderRepository.findById(id).orElseThrow(OrderNotFoundException::new);
    }

    @Override
    public List<Order> findAll() {
        return orderRepository.findAll();
    }

    @Override
    public void delete(Long id) {
        orderRepository.deleteById(id);
    }

    @Override
    public void deleteAll() {
        orderRepository.deleteAll();
    }
}
