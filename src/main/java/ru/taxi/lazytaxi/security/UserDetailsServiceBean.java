package ru.taxi.lazytaxi.security;

import java.util.stream.Collectors;

import ru.taxi.lazytaxi.repository.UserRepository;

import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

@Service
public class UserDetailsServiceBean implements UserDetailsService {
	private UserRepository userRepository;

	public UserDetailsServiceBean(UserRepository userRepository) {
		this.userRepository = userRepository;
	}

	@Override
	public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
		return userRepository
				.findByUsername(username)
				.map(u -> new org.springframework.security.core.userdetails.User(
						u.getUsername(),
						u.getPassword(),
						u.getEnabled(),
						u.getEnabled(),
						u.getEnabled(),
						u.getEnabled(),
						AuthorityUtils.createAuthorityList(
								u.getRoles().stream()
										.map(r -> "ROLE_" + r.getName().toUpperCase())
										.collect(Collectors.toList())
										.toArray(new String[]{}))))
				.orElseThrow(() -> new UsernameNotFoundException("No user with "
						+ "the name " + username + "was found in the database"));
	}

}