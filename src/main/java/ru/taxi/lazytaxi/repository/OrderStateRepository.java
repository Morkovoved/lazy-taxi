package ru.taxi.lazytaxi.repository;

import ru.taxi.lazytaxi.model.OrderState;

import org.springframework.data.jpa.repository.JpaRepository;

public interface OrderStateRepository extends JpaRepository<OrderState, Integer> {

}
