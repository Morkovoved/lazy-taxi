package ru.taxi.lazytaxi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.taxi.lazytaxi.model.Car;
import ru.taxi.lazytaxi.model.User;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface CarRepository extends JpaRepository<Car, Integer> {
    Optional<Car> findByCarNumber(String carNumber);
    List<Car> findByColor(String color);
    List<Car> findByBrand(String brand);
    List<Car> findByModel(String model);
    List<Car> findByModelLike(String Model);
    List<Car> findByBrandAndModel(String brand, String model);
    List<Car> findByYearGreaterThanEqual(Integer year);
    List<Car> findByUser(User user);
    List<Car> findByEnabled(Boolean enabled);
    List<Car> findByCreatedDateAfter(Date startDate);
}
