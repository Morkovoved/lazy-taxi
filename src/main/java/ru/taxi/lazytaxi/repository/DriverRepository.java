package ru.taxi.lazytaxi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.taxi.lazytaxi.model.Car;
import ru.taxi.lazytaxi.model.Driver;
import ru.taxi.lazytaxi.model.DriverState;

import java.util.List;
import java.util.Optional;

@Repository
public interface DriverRepository extends JpaRepository<Driver, Integer> {
    Optional<Driver> findByDriverLicense(String driverLicense);
    Optional<Driver> findByCar(Car car);
    List<Driver> findByDriverState(DriverState driverState);
    List<Driver> findByPriorityGreaterThan(Integer priority);
    List<Driver> findByRadiusWorkLessThan(Integer radiusWork);
    List<Driver> findByEnabled(Boolean enabled);
}
