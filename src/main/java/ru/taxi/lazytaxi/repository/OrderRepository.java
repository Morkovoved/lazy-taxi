package ru.taxi.lazytaxi.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import ru.taxi.lazytaxi.model.*;

import java.util.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface OrderRepository extends JpaRepository<Order, Long> {
    Optional<Order> findByClientPhone(String clientPhone);
    Optional<Order> findByDriver(Driver driver);
    Optional<Order> findByClient(User user);
    List<Order> findByOrderState(OrderState orderState);
    List<Order> findByPriorityGreaterThan(Integer priority);
    List<Order> findByAmountGreaterThan(Integer amount);
    List<Order> findByStartedDateAfter(Date startDate);
    List<Order> findByStartedDateBefore(Date startDate);
    List<Order> findByCreatedDateAfter(Date startDate);
}
