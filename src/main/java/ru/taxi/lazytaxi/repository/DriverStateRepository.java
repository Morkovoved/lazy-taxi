package ru.taxi.lazytaxi.repository;

import ru.taxi.lazytaxi.model.DriverState;

import org.springframework.data.jpa.repository.JpaRepository;

public interface DriverStateRepository extends JpaRepository<DriverState, Integer> {

}
