INSERT INTO "role"(role_id, name) VALUES (1, 'User');
INSERT INTO "role"(role_id, name) VALUES (2, 'Admin');

INSERT INTO driver_state(driver_state_id, name)	VALUES (1, 'Work');
INSERT INTO driver_state(driver_state_id, name)	VALUES (2, 'Busy');
INSERT INTO driver_state(driver_state_id, name)	VALUES (3, 'Free');
INSERT INTO driver_state(driver_state_id, name)	VALUES (4, 'Locked');

INSERT INTO order_state(order_state_id, name, final_state) VALUES (1, 'Free', false);
INSERT INTO order_state(order_state_id, name, final_state) VALUES (2, 'Fixed', false);
INSERT INTO order_state(order_state_id, name, final_state) VALUES (3, 'Assigned', false);
INSERT INTO order_state(order_state_id, name, final_state) VALUES (4, 'Waiting', false);
INSERT INTO order_state(order_state_id, name, final_state) VALUES (5, 'Executing', false);
INSERT INTO order_state(order_state_id, name, final_state) VALUES (6, 'Executed', true);
INSERT INTO order_state(order_state_id, name, final_state) VALUES (7, 'Refused', true);