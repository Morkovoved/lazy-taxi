package ru.taxi.lazytaxi.util;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.security.crypto.password.PasswordEncoder;
import ru.taxi.lazytaxi.model.*;

import java.util.Collections;
import java.util.Date;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;

public class TestUtils {

	public static Car createCar() {
		Car car = new Car();
		car.setCarNumber(randomAlphabetic(20));
		car.setColor(randomAlphabetic(20));
		car.setYear(nextInt(2000, 2019));
		car.setBrand(randomAlphabetic(20));
		car.setModel(randomAlphabetic(20));
		return car;
	}

	public static Driver createDriver(PasswordEncoder passwordEncoder) {
		Driver driver = createDriver();
		driver.setPassword(passwordEncoder.encode(randomAlphabetic(20)));
		return driver;
	}

	public static Driver createDriver() {
		Car car = new Car();
		car.setCarNumber(randomAlphabetic(20));
		car.setColor(randomAlphabetic(20));
		car.setYear(nextInt(2000, 2019));
		car.setBrand(randomAlphabetic(20));
		car.setModel(randomAlphabetic(20));

		DriverState driverState = new DriverState();
		driverState.setDriverStateId(nextInt(1, 4));
		driverState.setName(randomAlphabetic(20));

		Driver driver = new Driver();
		driver.setName(randomAlphabetic(20));
		driver.setSurname(randomAlphabetic(20));
		driver.setEmail(randomAlphabetic(20) + "@gmail.com");
		driver.setUsername(randomAlphabetic(20));
		driver.setPassword(randomAlphabetic(20));
		driver.setPassword(randomAlphabetic(20));
		driver.setPriority(nextInt(0, 2000));
		driver.setRadiusWork(nextInt(0, 500));
		driver.setCar(car);
		driver.setDriverLicense(randomAlphabetic(20));
		driver.setDriverState(driverState);
		return driver;
	}

	public static Order createOrder() {
		OrderState orderState = new OrderState();
		orderState.setName("Free");
		orderState.setOrderStateId(1);
		orderState.setFinalState(false);

		Order order = new Order();
		order.setClientPhone(randomNumeric(10));
		order.setStartedDate(new Date());
		order.setCreatedDate(new Date());
		order.setOrderState(orderState);
		order.setAmount(nextInt(0, 100000));
		order.setComment(randomAlphabetic(100));
		order.setPriority(1000);
		return order;
	}

	public static User createUser(PasswordEncoder passwordEncoder) {
		User user = createUser();
		user.setPassword(passwordEncoder.encode(randomAlphabetic(20)));
		return user;
	}

	public static User createUser() {
		Role role = new Role();
		role.setRoleId(1);
		role.setName("User");

		User user = new User();
		user.setName(randomAlphabetic(20));
		user.setSurname(randomAlphabetic(20));
		user.setEmail(randomAlphabetic(20) + "@gmail.com");
		user.setUsername(randomAlphabetic(20));
		user.setPassword(randomAlphabetic(20));
		user.setRoles(Collections.singleton(role));
		return user;
	}

	public static String asJsonString(final Object obj) {
		try {
			return new ObjectMapper().writeValueAsString(obj);
		} catch (Exception e) {
			throw new RuntimeException(e);
		}
	}
}
