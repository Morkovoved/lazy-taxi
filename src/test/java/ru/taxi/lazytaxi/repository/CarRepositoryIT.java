package ru.taxi.lazytaxi.repository;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.taxi.lazytaxi.model.Car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class CarRepositoryIT {
    private Car car;
    @Autowired
    private CarRepository carRepository;

    @Before
    public void init() {
        Car car = new Car();
        car.setCarNumber(randomAlphabetic(20));
        car.setColor(randomAlphabetic(20));
        car.setYear(nextInt(2000, 2019));
        car.setBrand(randomAlphabetic(20));
        car.setModel(randomAlphabetic(20));
        this.car = car;
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void save() {
        Car resultCar = carRepository.save(car);
        assertEquals(resultCar, car);
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete() {
        Car resultCar = carRepository.save(car);
        assertEquals(resultCar, car);

        carRepository.delete(resultCar);
        assertNull(carRepository.findById(resultCar.getCarId()).orElse(null));
    }

    @Test//(expected = NoResultException.class)
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void getCarById() {
        Car resultCar = carRepository.save(car);
        assertEquals(resultCar, car);

        Car receivedCar = carRepository.findById(resultCar.getCarId()).orElse(null);
        assertEquals(resultCar, receivedCar);
    }

    @Test
    public void getCars() {
        carRepository.findAll();
    }
}
