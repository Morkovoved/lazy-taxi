package ru.taxi.lazytaxi.repository;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import static org.junit.Assert.assertNotNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.taxi.lazytaxi.model.Car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class CarRepositoryLiteTest {
	private Car car;
	@Autowired
	private CarRepository carRepository;
	@Autowired
	private TestEntityManager entityManager;

	@Before
	public void init() {
		Car car = new Car();
		car.setCarNumber(randomAlphabetic(20));
		car.setColor(randomAlphabetic(20));
		car.setYear(nextInt(2000, 2019));
		car.setBrand(randomAlphabetic(20));
		car.setModel(randomAlphabetic(20));
		this.car = car;
	}

	@Test
	public void getCarById() {
		Car resultCar = entityManager.persist(car);
		assertNotNull(resultCar);
		Car receivedCar = carRepository.findById(resultCar.getCarId()).orElse(null);
		assertNotNull(receivedCar);
		assertEquals(resultCar, receivedCar);
	}

	@Test
	public void getCars() {
		carRepository.findAll();
	}
}
