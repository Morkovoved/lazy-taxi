package ru.taxi.lazytaxi.repository;

import java.util.Date;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.taxi.lazytaxi.model.Order;
import ru.taxi.lazytaxi.model.OrderState;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderRepositoryIT {
    private Order order;
    @Autowired
    private OrderRepository orderRepository;
    @Autowired
    private OrderStateRepository orderStateRepository;

    @Before
    public void init() {
//        orderStateRepository.save(new OrderState(1, "Free", false));
//        orderStateRepository.save(new OrderState(2, "Fixed", false));
//        orderStateRepository.save(new OrderState(3, "Assigned", false));
//        orderStateRepository.save(new OrderState(4, "Waiting", false));
//        orderStateRepository.save(new OrderState(5, "Executing", false));
//        orderStateRepository.save(new OrderState(6, "Executed", true));
//        orderStateRepository.save(new OrderState(7, "Refused", true));

        OrderState orderState = new OrderState();
        orderState.setName("Free");
        orderState.setOrderStateId(1);
        orderState.setFinalState(false);

//        OrderState orderState = orderStateRepository.findById(nextInt(1, 7)).orElseThrow();

        Order order = new Order();
        order.setClientPhone(randomNumeric(10));
//        order.setClient();
        order.setStartedDate(new Date());
        order.setCreatedDate(new Date());
        order.setOrderState(orderState);
        order.setAmount(nextInt(0, 100000));
        order.setComment(randomAlphabetic(100));
        order.setPriority(1000);
        this.order = order;
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void save() {
        Order resultOrder = orderRepository.save(order);
        assertEquals(resultOrder, order);
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete() {
        Order resultOrder = orderRepository.save(order);
        assertEquals(resultOrder, orderRepository.findById(order.getOrderId()).orElse(null));
        orderRepository.delete(resultOrder);
        assertNull(orderRepository.findById(resultOrder.getOrderId()).orElse(null));
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void getOrderById() {
        Order resultOrder = orderRepository.save(order);
        assertEquals(resultOrder, order);
        Order receivedOrder = orderRepository.findById(resultOrder.getOrderId()).orElse(null);
        assertEquals(resultOrder, receivedOrder);
    }

    @Test
    public void getOrders() {
        orderRepository.findAll();
    }
}
