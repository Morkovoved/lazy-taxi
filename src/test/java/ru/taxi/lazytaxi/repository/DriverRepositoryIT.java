package ru.taxi.lazytaxi.repository;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.taxi.lazytaxi.model.Car;
import ru.taxi.lazytaxi.model.Driver;
import ru.taxi.lazytaxi.model.DriverState;

import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DriverRepositoryIT {
    private Driver driver;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private DriverRepository driverRepository;
    @Autowired
    private DriverStateRepository driverStateRepository;

    @Before
    public void init() {
//        driverStateRepository.save(new DriverState(1, "Work"));
//        driverStateRepository.save(new DriverState(2, "Busy"));
//        driverStateRepository.save(new DriverState(3, "Free"));
//        driverStateRepository.save(new DriverState(4, "Locked"));

        Car car = new Car();
        car.setCarNumber(randomAlphabetic(20));
        car.setColor(randomAlphabetic(20));
        car.setYear(nextInt(2000, 2019));
        car.setBrand(randomAlphabetic(20));
        car.setModel(randomAlphabetic(20));

        DriverState driverState = new DriverState();
        driverState.setDriverStateId(nextInt(1, 4));
        driverState.setName(randomAlphabetic(20));

        Driver driver = new Driver();
        driver.setName(randomAlphabetic(20));
        driver.setSurname(randomAlphabetic(20));
        driver.setEmail(randomAlphabetic(20) + "@gmail.com");
        driver.setUsername(randomAlphabetic(20));
        driver.setPassword(randomAlphabetic(20));
        driver.setPassword(passwordEncoder.encode(randomAlphabetic(20)));
        driver.setPriority(nextInt(0, 2000));
        driver.setRadiusWork(nextInt(0, 500));
        driver.setCar(car);
        driver.setDriverLicense(randomAlphabetic(20));
        driver.setDriverState(driverState);
        this.driver = driver;
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void save() {
        Driver resultDriver = driverRepository.save(driver);
        assertEquals(resultDriver,driver);
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete() {
        Driver resultDriver = driverRepository.save(driver);
        assertEquals(resultDriver, driverRepository.findById(driver.getId()).orElse(null));
        driverRepository.delete(resultDriver);
        assertNull(driverRepository.findById(resultDriver.getId()).orElse(null));
    }

    @Test//(expected = NoResultException.class)
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void getDriverById() {
        Driver resultDriver = driverRepository.save(driver);
        assertNotNull(resultDriver);
        assertNotNull(resultDriver.getId());
        assertEquals(resultDriver, driverRepository.findById(driver.getId()).orElse(null));
    }

    @Test
    public void getDrivers() {
        driverRepository.findAll();
    }
}
