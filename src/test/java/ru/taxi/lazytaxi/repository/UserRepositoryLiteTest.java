package ru.taxi.lazytaxi.repository;

import java.util.Collections;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.taxi.lazytaxi.exception.UserNotFoundException;
import ru.taxi.lazytaxi.model.Role;
import ru.taxi.lazytaxi.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class UserRepositoryLiteTest {
	private User user;
	@Autowired
	private UserRepository userRepository;
	@Autowired
	private RoleRepository roleRepository;
	@Autowired
	private TestEntityManager entityManager;

	@Before
	public void init() {
        Role role = roleRepository.findById(nextInt(1, 2)).orElseThrow(UserNotFoundException::new);

		User user = new User();
		user.setName(randomAlphabetic(20));
		user.setSurname(randomAlphabetic(20));
		user.setEmail(randomAlphabetic(20) + "@gmail.com");
		user.setUsername(randomAlphabetic(20));
		user.setPassword(randomAlphabetic(20));
		user.setRoles(Collections.singleton(role));
		this.user = user;
	}

	@Test
	public void getUserById() {
		User resultUser = entityManager.persist(user);
		assertEquals(resultUser, userRepository.findById(user.getId()).orElse(null));
	}
}
