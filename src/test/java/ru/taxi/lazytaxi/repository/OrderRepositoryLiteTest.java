package ru.taxi.lazytaxi.repository;

import java.util.Date;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.taxi.lazytaxi.exception.OrderNotFoundException;
import ru.taxi.lazytaxi.model.Order;
import ru.taxi.lazytaxi.model.OrderState;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class OrderRepositoryLiteTest {
	private Order order;
	@Autowired
	private OrderRepository orderRepository;
	@Autowired
	private OrderStateRepository orderStateRepository;
	@Autowired
	private TestEntityManager entityManager;

	@Before
	public void init() {
        OrderState orderState = orderStateRepository.findById(nextInt(1, 7)).orElseThrow(OrderNotFoundException::new);
		Order order = new Order();
		order.setClientPhone(randomNumeric(10));
//        order.setClient();
		order.setStartedDate(new Date());
		order.setCreatedDate(new Date());
		order.setOrderState(orderState);
		order.setAmount(nextInt(0, 100000));
		order.setComment(randomAlphabetic(100));
		order.setPriority(1000);
		this.order = order;
	}

	@Test
	public void getOrderById() {
		Order resultOrder = entityManager.persist(order);
		assertEquals(resultOrder, order);
		Order receivedOrder = orderRepository.findById(resultOrder.getOrderId()).orElse(null);
		assertEquals(resultOrder, receivedOrder);
	}
}
