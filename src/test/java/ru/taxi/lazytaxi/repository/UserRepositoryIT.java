package ru.taxi.lazytaxi.repository;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import ru.taxi.lazytaxi.model.Role;
import ru.taxi.lazytaxi.model.User;

import java.util.Collections;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserRepositoryIT {
    private User user;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Before
    public void init() {
//        roleRepository.save(new Role(1, "User"));
//        roleRepository.save(new Role(2, "Admin"));

//        Role role = roleRepository.findById(nextInt(1, 2)).orElseThrow();
        Role role = new Role();
        role.setRoleId(1);
        role.setName("User");

        User user = new User();
        user.setName(randomAlphabetic(20));
        user.setSurname(randomAlphabetic(20));
        user.setEmail(randomAlphabetic(20) + "@gmail.com");
        user.setUsername(randomAlphabetic(20));
        user.setPassword(passwordEncoder.encode(randomAlphabetic(20)));
        user.setRoles(Collections.singleton(role));
        this.user = user;
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void save() {
        User resultUser = userRepository.save(user);
        assertEquals(resultUser, userRepository.findById(user.getId()).orElse(null));
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    @Sql(value = {"/data/test-create-user-data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    @Sql(value = {"/data/test-delete-user-data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
    public void update() {
        User resultUser = userRepository.findById(1).orElse(null);
        assertNotNull(resultUser);
        resultUser.setName("Sam");
//        userRepository.save(resultUser);
        assertEquals("Sam", userRepository.findById(resultUser.getId()).orElse(new User()).getName());
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void delete() {
        User resultUser = userRepository.save(user);
        assertEquals(resultUser, userRepository.findById(user.getId()).orElse(null));
        userRepository.delete(resultUser);
        assertNull(userRepository.findById(resultUser.getId()).orElse(null));
    }

    @Test
    @Transactional(propagation = Propagation.REQUIRED, rollbackFor = Exception.class)
    public void getUserById() {
        User resultUser = userRepository.save(user);
        assertEquals(resultUser, userRepository.findById(user.getId()).orElse(null));
    }

    @Test
    public void getUsers() {
        userRepository.findAll();
    }
}
