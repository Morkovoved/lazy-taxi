package ru.taxi.lazytaxi.repository;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import ru.taxi.lazytaxi.model.Car;
import ru.taxi.lazytaxi.model.Driver;
import ru.taxi.lazytaxi.model.DriverState;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.boot.test.autoconfigure.orm.jpa.TestEntityManager;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@DataJpaTest
public class DriverRepositoryLiteTest {
	private Driver driver;
	@Autowired
	private DriverRepository driverRepository;
	@Autowired
	private TestEntityManager entityManager;

	@Before
	public void init() {
		Car car = new Car();
		car.setCarNumber(randomAlphabetic(20));
		car.setColor(randomAlphabetic(20));
		car.setYear(nextInt(2000, 2019));
		car.setBrand(randomAlphabetic(20));
		car.setModel(randomAlphabetic(20));

		DriverState driverState = new DriverState();
		driverState.setDriverStateId(nextInt(1, 4));
		driverState.setName(randomAlphabetic(20));

		Driver driver = new Driver();
		driver.setName(randomAlphabetic(20));
		driver.setSurname(randomAlphabetic(20));
		driver.setEmail(randomAlphabetic(20) + "@gmail.com");
		driver.setUsername(randomAlphabetic(20));
		driver.setPassword(randomAlphabetic(20));
		driver.setPriority(nextInt(0, 2000));
		driver.setRadiusWork(nextInt(0, 500));
		driver.setCar(car);
		driver.setDriverLicense(randomAlphabetic(20));
		driver.setDriverState(driverState);
		this.driver = driver;
	}

	@Test
	public void getDriverById() {
		Driver resultDriver = entityManager.persist(driver);
		assertEquals(resultDriver, driverRepository.findById(driver.getId()).orElse(null));
	}
}
