package ru.taxi.lazytaxi.security;


import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import static org.springframework.test.util.AssertionErrors.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.taxi.lazytaxi.util.TestUtils.asJsonString;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.taxi.lazytaxi.LazyTaxiApplication;
import ru.taxi.lazytaxi.model.User;
import ru.taxi.lazytaxi.repository.UserRepository;
import ru.taxi.lazytaxi.util.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
		classes = {LazyTaxiApplication.class, TestConfig.class})
@AutoConfigureMockMvc
//@TestPropertySource(locations = "classpath:application-integrationtest.properties")
@Sql(value = {"/data/test-create-user-data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
@Sql(value = {"/data/test-delete-user-data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class AuthorisationIT {
	private User user;
	@Autowired
	private MockMvc mvc;

	@Autowired
	private UserRepository userRepository;

	@Before
	public void init() {
		user = userRepository.saveAndFlush(TestUtils.createUser());
	}

	@Test
//	@WithUserDetails("StanlyMarch")
	public void shouldAllowUserWithNoAuthorities() throws Exception {
		mvc.perform(get("/users/" + + user.getId())
				.accept(MediaType.ALL))
//                .contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isOk());
	}

	@Test
//	@WithUserDetails("EricCartman")
	public void shouldRejectUserWithNoAuthorities() throws Exception {
		mvc.perform(get("/users/" + user.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isForbidden());
//                .andExpect(jsonPath("$.greetings", is("Welcome Seb (StanlyMarch)!")));
	}

	@Test
	public void shouldRejectIfNoAuthentication() throws Exception {
		mvc.perform(get("/users/" + user.getId())
				.contentType(MediaType.APPLICATION_JSON))
				.andDo(print())
				.andExpect(status().isUnauthorized());
	}
}
