package ru.taxi.lazytaxi.security;

import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Primary;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

import java.util.Arrays;
import java.util.Collections;

//@TestConfiguration
public class TestConfig {

    @Bean
    @Primary
    public UserDetailsService userDetailsService() {
        User basicUser1 = new org.springframework.security.core.userdetails.User(
                "StanlyMarch",
                "password",
                Collections.singletonList(new SimpleGrantedAuthority("ROLE_USER")));

        User basicUser2 = new org.springframework.security.core.userdetails.User(
                "EricCartman",
                "password",
                Collections.emptyList());

        return new InMemoryUserDetailsManager(basicUser1, basicUser2);
    }
}
