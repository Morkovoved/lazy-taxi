package ru.taxi.lazytaxi.service;

import java.util.Optional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import ru.taxi.lazytaxi.model.Car;
import ru.taxi.lazytaxi.repository.CarRepository;

import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.taxi.lazytaxi.util.TestUtils;

@RunWith(SpringRunner.class)
public class CarServiceLiteTest {
	private Car car;
	private CarService carService;

//	@Autowired
//	public void setCarService() {
//		this.carService = new CarServiceBean(carRepository);
//	}

//	@TestConfiguration
//	static class CarServiceLiteTestContextConfiguration {
//		@Bean
//		public CarService employeeService() {
//			return new CarServiceBean(carRepository);
//		}
//	}

	@MockBean
	private CarRepository carRepository;

	@Before
	public void init() {
		car = TestUtils.createCar();
		car.setCarId(nextInt(0, 2000));
		Mockito.when(carRepository.findById(car.getCarId()))
				.thenReturn(Optional.of(car));
		Mockito.when(carRepository.existsById(car.getCarId()))
				.thenReturn(true);
		carService = new CarServiceBean(carRepository);
	}

	@Test
	public void getCarById() {
		assertEquals(car, carService.find(car.getCarId()));
	}
}
