package ru.taxi.lazytaxi.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.taxi.lazytaxi.exception.OrderNotFoundException;
import ru.taxi.lazytaxi.model.Order;
import ru.taxi.lazytaxi.model.OrderState;
import ru.taxi.lazytaxi.repository.OrderStateRepository;
import ru.taxi.lazytaxi.util.TestUtils;

import java.util.Date;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomStringUtils.randomNumeric;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class OrderServiceIT {
    private Order order;
    @Autowired
    private OrderService orderService;

    @Before
    public void init() {
        order = TestUtils.createOrder();
    }

    @Test
    public void save() {
        Order resultOrder = orderService.save(order);
        assertEquals(resultOrder, order);
    }

    @Test(expected = OrderNotFoundException.class)
    public void delete() {
        Order resultOrder = orderService.save(order);
        orderService.delete(resultOrder.getOrderId());
        orderService.find(resultOrder.getOrderId());
    }

    @Test
    public void getOrderById() {
        Order resultOrder = orderService.save(order);
        assertEquals(resultOrder, orderService.find(resultOrder.getOrderId()));
    }

    @Test
    public void getOrders() {
        orderService.findAll();
    }
}
