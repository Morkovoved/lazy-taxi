package ru.taxi.lazytaxi.service;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.jdbc.Sql;
import ru.taxi.lazytaxi.model.Car;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import ru.taxi.lazytaxi.exception.CarNotFoundException;
import ru.taxi.lazytaxi.util.TestUtils;

@RunWith(SpringRunner.class)
@SpringBootTest
//@TestPropertySource(locations = "classpath:application-test.properties")
public class CarServiceIT {
	private Car car;
	@Autowired
	private CarService carService;

	@Before
	public void init() {
		this.car = TestUtils.createCar();
	}

	@Test
	public void save() {
		Car resultCar = carService.save(car);
		assertEquals(resultCar, car);
	}

	@Test(expected = CarNotFoundException.class)
	public void delete() {
		Car resultCar = carService.save(car);
		assertEquals(resultCar, car);

		carService.delete(resultCar.getCarId());
		carService.find(resultCar.getCarId());
	}

	@Test(expected = CarNotFoundException.class)
	@Sql(value = {"/data/test-create-user-data.sql", "/data/test-create-car-data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
	@Sql(value = {"/data/test-delete-user-data.sql", "/data/test-delete-car-data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
	public void deleteCar() {
		carService.delete(1);
		carService.find(1);
	}

	@Test
	public void getCarById() {
		Car resultCar = carService.save(car);
		assertEquals(resultCar, car);

		Car receivedCar = carService.find(resultCar.getCarId());
		assertEquals(resultCar, receivedCar);
	}

	@Test
	public void getCars() {
		carService.findAll();
	}
}
