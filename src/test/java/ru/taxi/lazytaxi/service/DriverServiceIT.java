package ru.taxi.lazytaxi.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.taxi.lazytaxi.exception.DriverNotFoundException;
import ru.taxi.lazytaxi.model.Car;
import ru.taxi.lazytaxi.model.Driver;
import ru.taxi.lazytaxi.model.DriverState;
import ru.taxi.lazytaxi.repository.DriverStateRepository;
import ru.taxi.lazytaxi.util.TestUtils;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

@RunWith(SpringRunner.class)
@SpringBootTest
public class DriverServiceIT {
    private Driver driver;
    @Autowired
    private DriverService driverService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    public void init() {
        driver = TestUtils.createDriver(passwordEncoder);
    }

    @Test
    public void save() {
        Driver resultDriver = driverService.save(driver);
        assertEquals(resultDriver, driver);
    }

    @Test(expected = DriverNotFoundException.class)
    public void delete() {
        Driver resultDriver = driverService.save(driver);
        driverService.delete(resultDriver.getId());
        driverService.find(resultDriver.getId());
    }

    @Test
    @Transactional
    public void getDriverById() {
        Driver resultDriver = driverService.save(driver);
        assertEquals(resultDriver, driverService.find(driver.getId()));
    }

    @Test
    public void getDrivers() {
        driverService.findAll();
    }
}
