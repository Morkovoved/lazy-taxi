package ru.taxi.lazytaxi.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.taxi.lazytaxi.model.User;
import ru.taxi.lazytaxi.repository.UserRepository;
import ru.taxi.lazytaxi.util.TestUtils;

import java.util.Optional;

import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class UserServiceLiteTest {
    private User user;
    private UserService userService;
    @MockBean
    private UserRepository userRepository;

    @Before
    public void init() {
        user = TestUtils.createUser();
        user.setId(nextInt(0, 2000));
        Mockito.when(userRepository.findById(user.getId()))
                .thenReturn(Optional.of(user));
        userService = new UserServiceBean(userRepository);
    }

    @Test
    public void getUserById() {
        assertEquals(user, userService.find(user.getId()));
    }
}
