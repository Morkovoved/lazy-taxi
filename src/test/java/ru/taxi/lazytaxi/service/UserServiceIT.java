package ru.taxi.lazytaxi.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.test.context.jdbc.Sql;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import ru.taxi.lazytaxi.exception.UserNotFoundException;
import ru.taxi.lazytaxi.model.User;
import ru.taxi.lazytaxi.util.TestUtils;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.junit.Assert.*;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserServiceIT {
    private User user;
    @Autowired
    private UserService userService;
    @Autowired
    private PasswordEncoder passwordEncoder;

    @Before
    public void init() {
        user = TestUtils.createUser(passwordEncoder);
    }

    @Test
    public void save() {
        User resultUser = userService.save(user);
        assertEquals(resultUser, userService.find(user.getId()));
    }

    @Test(expected = UserNotFoundException.class)
    public void delete() {
        User resultUser = userService.save(user);
        userService.delete(resultUser.getId());
        userService.find(resultUser.getId());
    }

    @Test
    public void getUserById() {
        User resultUser = userService.save(user);
        assertEquals(resultUser, userService.find(user.getId()));
    }

    @Test
    public void getUsers() {
        userService.findAll();
    }
}
