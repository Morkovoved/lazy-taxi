package ru.taxi.lazytaxi.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.taxi.lazytaxi.model.Order;
import ru.taxi.lazytaxi.repository.OrderRepository;
import ru.taxi.lazytaxi.util.TestUtils;

import java.util.Optional;

import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.apache.commons.lang3.RandomUtils.nextLong;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class OrderServiceLiteTest {
    private Order order;
    private OrderService orderService;
    @MockBean
    private OrderRepository orderRepository;

    @Before
    public void init() {
        order = TestUtils.createOrder();
        order.setOrderId(nextLong(0, 2000));
        Mockito.when(orderRepository.findById(order.getOrderId()))
                .thenReturn(Optional.of(order));
        orderService = new OrderServiceBean(orderRepository);
    }

    @Test
    public void getOrderById() {
        assertEquals(order, orderService.find(order.getOrderId()));
    }
}
