package ru.taxi.lazytaxi.service;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import ru.taxi.lazytaxi.model.Car;
import ru.taxi.lazytaxi.model.Driver;
import ru.taxi.lazytaxi.repository.DriverRepository;
import ru.taxi.lazytaxi.util.TestUtils;

import java.util.Optional;

import static org.apache.commons.lang3.RandomStringUtils.randomAlphabetic;
import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.junit.Assert.assertEquals;

@RunWith(SpringRunner.class)
public class DriverServiceLiteTest {
    private Driver driver;
    private DriverService driverService;
    @MockBean
    private DriverRepository driverRepository;

    @Before
    public void init() {
        driver = TestUtils.createDriver();
        driver.setId(nextInt(0, 2000));
        Mockito.when(driverRepository.findById(driver.getId()))
                .thenReturn(Optional.of(driver));
        driverService = new DriverServiceBean(driverRepository);
    }

    @Test
    public void getDriverById() {
        assertEquals(driver, driverService.find(driver.getId()));
    }
}
