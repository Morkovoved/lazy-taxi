package ru.taxi.lazytaxi.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.security.test.context.support.WithUserDetails;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.taxi.lazytaxi.LazyTaxiApplication;
import ru.taxi.lazytaxi.exception.UserNotFoundException;
import ru.taxi.lazytaxi.security.TestConfig;
import ru.taxi.lazytaxi.model.User;
import ru.taxi.lazytaxi.repository.UserRepository;
import ru.taxi.lazytaxi.util.TestUtils;

import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertEquals;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.taxi.lazytaxi.util.TestUtils.asJsonString;

//@RunWith(SpringRunner.class)
//@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT,
//        classes = {LazyTaxiApplication.class, TestConfig.class})
//@AutoConfigureMockMvc
//@WithMockUser
//@TestPropertySource(locations = "classpath:application-integrationtest.properties")
//@Sql(value = {"/data/test-create-user-data.sql"}, executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
//@Sql(value = {"/data/test-delete-user-data.sql"}, executionPhase = Sql.ExecutionPhase.AFTER_TEST_METHOD)
public class UserControllerMockIT {
    private User user;
    @Autowired
    private MockMvc mvc;

    @Autowired
    private UserRepository userRepository;

    @Before
    public void init() {
        user = TestUtils.createUser();
    }

//    @Test
//    @WithUserDetails("StanlyMarch")
    public void create() throws Exception {
        mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user)))
                .andDo(print())
                .andExpect(status().isCreated());

    }

//    @Test
//    @WithUserDetails("StanlyMarch")
    public void update() throws Exception {
        user = userRepository.saveAndFlush(user);

        User updatedUser = new User();
        updatedUser.setId(user.getId());
        updatedUser.setUsername(user.getUsername());
        updatedUser.setPassword(user.getPassword());
        updatedUser.setEmail("newEmail@gmail.com");
        updatedUser.setName("newName");
        updatedUser.setSurname(user.getSurname());
        updatedUser.setRoles(user.getRoles());
        updatedUser.setCars(user.getCars());
        updatedUser.setCreatedDate(user.getCreatedDate());
        updatedUser.setEnabled(user.getEnabled());

        mvc.perform(put("/users/" + updatedUser.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(updatedUser)))
                .andDo(print())
                .andExpect(status().isNoContent());

        user = userRepository.findById(user.getId()).orElseThrow(UserNotFoundException::new);
        assertEquals(user, updatedUser);
    }

//    @Test
//    @WithUserDetails("StanlyMarch")
    public void getUser() throws Exception {
//        user = userRepository.saveAndFlush(user);

        mvc.perform(get("/users/" + 1)
                .accept(MediaType.ALL))
//                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private User setId(User user, Integer userId) {
        user.setId(userId);
        return user;
    }
}
