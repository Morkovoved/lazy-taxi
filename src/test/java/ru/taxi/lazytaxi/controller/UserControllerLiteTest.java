package ru.taxi.lazytaxi.controller;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import ru.taxi.lazytaxi.model.User;
import ru.taxi.lazytaxi.service.UserService;
import ru.taxi.lazytaxi.util.TestUtils;

import static org.apache.commons.lang3.RandomUtils.nextInt;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static ru.taxi.lazytaxi.util.TestUtils.asJsonString;

@RunWith(SpringRunner.class)
@WebMvcTest(UserController.class)
@WithMockUser
public class UserControllerLiteTest {
    private User user;
    @Autowired
    private MockMvc mvc;
    @MockBean
    private UserService userService;
    @MockBean
    private UserDetailsService userDetailsService;
    @MockBean
    private PasswordEncoder passwordEncoder;

    @Before
    public void init() {
        user = TestUtils.createUser();
    }

    @Test
    public void create() throws Exception {
        Mockito.when(userService.save(user)).thenReturn(setId(user, nextInt(0, 2000)));

        mvc.perform(post("/users")
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user)))
                .andDo(print())
                .andExpect(status().isCreated());

    }

    @Test
    public void update() throws Exception {
        user.setId(nextInt(0, 2000));
        Mockito.when(userService.save(user)).thenReturn(user);

        mvc.perform(put("/users/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(asJsonString(user)))
                .andDo(print())
                .andExpect(status().isNoContent());
    }

    @Test
    public void getUser() throws Exception {
        user.setId(nextInt(0, 2000));
        Mockito.when(userService.find(user.getId())).thenReturn(user);

        mvc.perform(get("/users/" + user.getId())
                .contentType(MediaType.APPLICATION_JSON))
                .andDo(print())
                .andExpect(status().isOk());
    }

    private User setId(User user, Integer userId) {
        user.setId(userId);
        return user;
    }
}
